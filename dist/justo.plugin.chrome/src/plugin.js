"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.open = undefined;

var _justo = require("justo");

//tasks
const open = exports.open = (0, _justo.simple)({
  id: "open",
  desc: "Open Google Chrome.",
  fmt(params) {
    if (params.src) return "Open Google Chrome: " + params.src;else return "Open Google Chrome";
  }
}, require("./open").default); //imports