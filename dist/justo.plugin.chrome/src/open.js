"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = open;

var _os = require("os");

var _os2 = _interopRequireDefault(_os);

var _path = require("path");

var _path2 = _interopRequireDefault(_path);

var _child_process = require("child_process");

var _child_process2 = _interopRequireDefault(_child_process);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

//Open Google Chrome.
function open(params) {
  var cmd,
      args = [];

  //(1) determine command
  if (/^win/.test(_os2.default.platform())) cmd = "chrome.exe";else cmd = "google-chrome";

  if (params.path) cmd = _path2.default.join(params.path, cmd);
  if (params.newWin) args.push("--new-window");
  if (params.incognito) args.push("--incognito");
  if (params.src) args.push(params.src);

  //(2) open
  _child_process2.default.spawn(cmd, args, { detached: true, stdio: "ignore" }).unref();
} //imports