# justo-plugin-chrome

**Justo** plugin for **Google Chrome**.

*Proudly made with ♥ in Valencia, Spain, EU.*

## Use

```
const chrome = require("justo.plugin.chrome");
```

## open task

This task opens **Google Chrome**:

```
chrome.open({path, src, newWin, incognito});
```

- `path` (string). Directory where **Chrome** is. Needed when it is not in the PATH.
- `src` (string). The file or resource to open.
- `newWin` (bool). Open in new window? Default: `false`.
- `incognito` (bool). Open in new window in incognito mode. Default: `false`.

Example:

```
chrome.open({
  src: "http://dogmalang.com",
  newWin: true
});
```
