//imports
import {simple} from "justo";

//tasks
export const open = simple(
  {
    id: "open",
    desc: "Open Google Chrome.",
    fmt(params) {
      if (params.src) return "Open Google Chrome: " + params.src;
      else return "Open Google Chrome";
    }
  },

  require("./open").default
);
