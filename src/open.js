//imports
import os from "os";
import path from "path";
import child_process from "child_process";

//Open Google Chrome.
export default function open(params) {
  var cmd, args = [];

  //(1) determine command
  if (/^win/.test(os.platform())) cmd = "chrome.exe";
  else cmd = "google-chrome";

  if (params.path) cmd = path.join(params.path, cmd);
  if (params.newWin) args.push("--new-window");
  if (params.incognito) args.push("--incognito");
  if (params.src) args.push(params.src);

  //(2) open
  child_process.spawn(cmd, args, {detached: true, stdio: "ignore"}).unref();
}
