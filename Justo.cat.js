//imports
const {catalog} = require("justo");
const babel = require("justo.plugin.babel");
const cli = require("justo.plugin.cli");
const eslint = require("justo.plugin.eslint");
const fs = require("justo.plugin.fs");
const npm = require("justo.plugin.npm");

//internal data
const PKG = "justo.plugin.chrome";

//catalog
catalog.call("lint", eslint, {
  src: "."
}).title("Lint source code");

catalog.macro("trans-js", [
  {task: babel, params: {src: "src", dst: `dist/${PKG}/src/`}}
]).title("Transpile from JS to JS");

catalog.macro("build", [
  {task: catalog.get("trans-js")},
  {task: fs.copy, params: {src: "package.json", dst: `dist/${PKG}/package.json`}},
  {task: fs.copy, params: {src: "README.md", dst: `dist/${PKG}/README.md`}},
]).title("Build package");

catalog.macro("make", [
  {task: fs.rm, params: {path: "./dist"}},
  catalog.get("lint"),
  catalog.get("build")
]).title("Lint and build");

catalog.call("install", npm.install, {
  pkg: `dist/${PKG}`,
  global: true
}).title("Install package globally");

catalog.call("pub", npm.publish, {
  who: "justojs",
  path: `dist/${PKG}`
}).title("Publish in NPM");

catalog.macro("dflt", [
  catalog.get("make")
]).title("Lint and build");
